{-# LANGUAGE BangPatterns #-}

import Control.Monad (foldM, replicateM)
import Control.Monad.ST
import Data.STRef

data Treap = Empty | Treap {
    x :: Int,
    y :: Int,
    right :: Treap,
    left :: Treap,
    lcount :: Int,
    rcount :: Int
} deriving Show

treap x y = Treap { rcount = 0, lcount = 0, x = x, y = y, left = Empty, right = Empty }

count Empty = 0
count t = 1 + lcount t + rcount t

split Empty _ = (Empty, Empty)
split t k
  | k > (x t) = (t { right = t1, rcount = count t1 }, t2)
  | otherwise = (t1', t { left = t2', lcount = count t2' })
    where
      (t1, t2) = split (right t) k
      (t1', t2') = split (left t) k

merge Empty t2 = t2
merge t1 Empty = t1
merge t1 t2
  | (y t1) > (y t2) = t1 { right = rm, rcount = count rm }
  | otherwise = t2 { left = lm, lcount = count lm }
    where
      rm = merge (right t1) t2
      lm = merge t1 (left t2)

insert t !y !x = merge t1' t2 where
  (t1, t2) = split t x
  t1' = merge t1 (treap x y)

index t !k = indexC t k 0 where
  indexC Empty _ c = c
  indexC t !k !c
    | k >= (x t) = indexC (right t) k (c + 1 + lcount t)
    | otherwise = indexC (left t) k c

parseLine :: String -> (Char, Int)
parseLine (t:digits) = (t, read digits)

look t !i = look' t i 0 where
  look' Empty i l = i + l
  look' !t !i !l
    | i < f = look' (left t) i l
    | otherwise = look' (right t) (i-f) (x t + 1)
      where f = x t - l - (count $ left t)

harry t ('L', i) = do
  print $ look t i
  return t

harry t ('D', i) = do
  let y = (i * 8501) `rem` 9973
  let x = look t i
  return $ insert t y x

main = do
  [n, m] <- (map read . words) `fmap` getLine :: IO [Int]
  replicateM m (parseLine `fmap` getLine) >>= foldM harry Empty

{-# LANGUAGE BangPatterns #-}

import qualified Control.Monad as M
import qualified Data.Sequence as Q
import qualified Data.ByteString.Char8 as S
import Data.Char (ord)
import Data.IORef
import System.IO.Unsafe (unsafePerformIO)
import Prelude hiding (id)
import Data.Maybe (fromJust, isJust)

gCHILDREN = 128

_TRIES :: IORef (Q.Seq Trie)
_TRIES = unsafePerformIO . newIORef . Q.singleton $ r where
  r = Trie { id = 0, code = -1, parent = 0, suffixLink = 0, isLeaf = False, children = Q.replicate gCHILDREN 0, depth = 0, nearestLink = 0 }

root :: IO Trie
root = at 0

trie i c l p d = Trie {
  id = i,
  depth = d,
  code = c,
  isLeaf = l,
  children = Q.replicate gCHILDREN (-1),
  parent = id p,
  nearestLink = -1,
  suffixLink = -1
}

addChild t x l d = do
  let ci = children t `Q.index` x
  let t' nc = t { children = Q.update x (id nc) (children t) }
  if (ci <= 0) then do
    ci' <- Q.length `fmap` readIORef _TRIES
    let !nc = trie ci' x l t d
    modifyIORef' _TRIES (Q.|> nc)
    modifyIORef' _TRIES $ Q.update (id t) (t' nc)
    return nc
  else do
    c <- at ci
    modifyIORef' _TRIES $ Q.update ci c { isLeaf = True }
    modifyIORef' _TRIES $ Q.update (id t) (t' c)
    return c

addWord :: [Int] -> Trie -> IO Trie
addWord xs t = addWord' xs 0 t where
  addWord' [x] !d t = addChild t x True d
  addWord' (x:xs) !d t = addChild t x False d >>= addWord' xs (d+1)

at :: Int -> IO Trie
at i = (\x -> Q.index x i) `fmap` readIORef _TRIES

suffixLink2 :: Trie -> IO Trie
suffixLink2 t = do
  let s = suffixLink t
  if (s < 0) then do
    ts <- go (code t) =<< suffixLink2 =<< parent2 t
    let s' = if (parent t == 0) then 0 else id ts
    modifyIORef' _TRIES $ Q.update (id t) t { suffixLink = s' }
    at s'
  else at s

nearestLink2 t = do
  let n = nearestLink t
  if (n < 0) then do
    let find t = if (isLeaf t || id t == 0) then return t else suffixLink2 t >>= find
    n' <- find t
    modifyIORef' _TRIES $ Q.update (id t) t { nearestLink = id n' }
    return n'
  else at n

parent2 = at . parent

go :: Int -> Trie -> IO Trie
go i t = do
  let n = children t `Q.index` i
  if (n >= 0) then at n
  else suffixLink2 t >>= go i

data Trie = Trie {
  id :: Int,
  depth :: Int,
  code :: Int,
  parent :: Int,
  suffixLink :: Int,
  nearestLink :: Int,
  isLeaf :: Bool,
  children :: Q.Seq Int
} deriving Show

readInt x = go $ S.readInt x where
  go (Just a) = fst a

occurence :: [Int] -> IO (Maybe Int)
occurence xs = root >>= \r -> oc xs 0 r r where
  oc _ i _ t@(Trie { isLeaf=True }) = return $ Just (i - depth t)
  oc [] i l@(Trie { isLeaf=True }) _ = return $ Just (i - depth l)
  oc [] _ _ _ = return Nothing
  oc (x:xs) !i l t = do
    nl <- nearestLink2 t
    if (id nl > 0)
    then go x t >>= oc xs (i+1) (nl{depth=depth nl + 1})
    else go x t >>= oc xs (i+1) (l{depth=depth l + 1})

main = do
  let addWord' xs = root >>= addWord xs
  let getOrdLine = map ord `fmap` getLine
  n <- readInt `fmap` S.getLine
  M.replicateM_ n ((map (map ord) . words) `fmap` getLine >>= mapM_ addWord')
  m <- readInt `fmap` S.getLine
  -- print =<< readIORef _TRIES
  ocs <- mapM occurence =<< M.replicateM m (map ord `fmap` getLine)
  let answer = take 1 $ filter (isJust . snd) (zip [1..] ocs)
  if (length answer == 1) then do
    let [(a, Just b)] = answer
    putStrLn . unwords . map show $ [a, b]
  else putStrLn "Passed"

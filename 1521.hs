{-# LANGUAGE BangPatterns #-} 

data Treap = Empty | Treap {
    x :: Int,
    y :: Int,
    right :: Treap,
    left :: Treap,
    lcount :: Int,
    rcount :: Int
} deriving Show

treap !x !y = Treap { rcount = 0, lcount = 0, x = x, y = y, left = Empty, right = Empty }

count Empty = 0
count t = 1 + (lcount t) + (rcount t)

split Empty _ = (Empty, Empty)
split t k 
  | k > (x t) = (t { right = t1, rcount = count t1 }, t2)
  | otherwise = (t1', t { left = t2', lcount = count t2' })
    where
      (t1, t2) = split (right t) k
      (t1', t2') = split (left t) k

merge Empty t2 = t2
merge t1 Empty = t1
merge t1 t2
  | (y t1) > (y t2) = t1 { right = rm, rcount = count rm }
  | otherwise = t2 { left = lm, lcount = count lm }
    where
      rm = merge (right t1) t2
      lm = merge t1 (left t2)

remove t k = merge t1 $ t2 `lremove` k
  where
    lremove t k 
      | (x t) == k = right t 
      | otherwise = t { left = left t `lremove` k, lcount = lcount t - 1 }
    (t1, t2) = split t k
    
at t !i | i == c = x t | i > c = at (right t) $ i `seq` (i-c-1) | i < c = at (left t) i
  where c = lcount t
 
mktree !s !e !p
  | s > e = Empty
  | s == e = treap s p
  | otherwise = Treap { left = l, right = r, x = m, y = p, lcount = (m-s), rcount = (e-m) }
  where 
    m = (s+e) `div` 2
    p' = p - 1
    l = mktree s (m-1) p'
    r = mktree (m+1) e p'

main = do
  [n, k] <- (map read . words) `fmap` getLine :: IO [Int]
  let
    t' = mktree 1 n n
    k' = k - 1
    round' (l, i, t, _) = (l-1, ni, remove t e, e) 
      where ni = (k' + i) `mod` l; e = at t ni 
    trees = iterate round' (n, 0, t', 0)
    order = take n . drop 1 . map (\(_,_,_,i) -> i)
  putStrLn . unwords . map show . order $ trees